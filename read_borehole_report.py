from PyPDF2 import PdfFileReader  # pip install PyPDF2
import string
import configparser
import os
import time
import concurrent.futures

import requests  # pip install requests
from pdf2image import convert_from_path  # pip install pdf2image
import pytesseract  # pip install pytesseract - requires tesseract install on pc and danish language pack
import PIL
import pandas as pd
from sqlalchemy import create_engine
import psycopg2 as pg


class Database:

    def __init__(self):
        self.ini_dir = r'F:/GKO/data/grukos/db_credentials/writer/writer.ini'
        self.ini_section = 'JUPITER'
        self.user, self.password, self.host, self.port, self.database \
            = self.parse_db_credentials()
        self.engine = self.create_engine()
        self.df_export = pd.DataFrame()

    def connect_to_pg_db(self):
        try:
            pg_con = pg.connect(
                host=self.host,
                port=self.port,
                database=self.database,
                user=self.user,
                password=self.password
            )
            print('Connected to database: ' + self.database)
            return pg_con
        except Exception as e:
            print('Unable to connect to database: ' + self.database)
            print(e)

    def parse_db_credentials(self):
        config = configparser.ConfigParser()
        config.read(self.ini_dir)

        password = config[f'{self.ini_section}']['password']
        user = config[f'{self.ini_section}']['userid']
        host = config[f'{self.ini_section}']['host']
        port = config[f'{self.ini_section}']['port']
        database = config[f'{self.ini_section}']['databasename']

        return user, password, host, port, database

    def fetch_report_urls(self, chunk):
        sql = f'''
            SELECT DISTINCT ON (bd.guid)
                b.boreholeid,
                sd.url,
                bd.guid AS guid_boredoc,
                bd.guid_borehole
            FROM jupiter.storedoc sd
            LEFT JOIN jupiter.boredoc bd USING (fileid)
            LEFT JOIN jupiter.borehole b USING (boreholeid) 
            LEFT JOIN temp_simak.color_report_test crt USING (boreholeid)
            WHERE b.geom IS NOT NULL
                AND bd.doctype IN ('B', 'BG')
                AND crt.guid_boredoc IS NULL
            ORDER BY bd.guid, sd.insertdate DESC NULLS LAST 
            LIMIT {str(chunk)}
            ;
        '''

        sql_2 = f'''
            SELECT DISTINCT on (bd.guid)
                b.boreholeid,
                sd.url,
                bd.guid AS guid_boredoc,
                bd.guid_borehole
            FROM jupiter.storedoc sd
            LEFT JOIN jupiter.boredoc bd USING (fileid)
            LEFT JOIN jupiter.borehole b USING (boreholeid) 
            WHERE b.geom IS NOT NULL
                AND bd.doctype IN ('B', 'BG')
            ORDER BY bd.guid, sd.insertdate DESC NULLS LAST 
            LIMIT {str(chunk)}
            ;
        '''

        with self.connect_to_pg_db() as con:
            try:
                df = pd.read_sql_query(sql, con=con)
            except Exception as e:
                print(e)
                df = pd.read_sql_query(sql_2, con=con)

        return df

    def create_engine(self):
        con = create_engine(f'postgresql://{self.user}:{self.password}@{self.host}:{self.port}/{self.database}')
        return con

    def fetch_color_from_report(self, df):
        foldername = './temp'
        if not os.path.exists(foldername):
            os.makedirs(foldername)
        colors_lst = []
        has_text_lst = []
        has_color_lst = []

        for j, row in df.iterrows():
            url = row['url']
            print(url)
            id = row['guid_boredoc']
            pdf_name = f'{foldername}/{id}.pdf'
            png_name = f'{foldername}/{id}.png'
            response = requests.get(url, stream=True, allow_redirects=True)
            if response.status_code == 200:
                with open(pdf_name, 'wb') as f:
                    f.write(response.content)

                has_text = 0
                has_color = 0
                bh_color_lst = []
                try:
                    pages = convert_from_path(pdf_name, 500, poppler_path=r'./poppler-0.68.0/bin')
                except Exception as e:
                    print(e)
                    bh_color_lst.append(None)
                else:
                    for i, page in enumerate(pages):
                        page.save(png_name, 'PNG')

                        text = str(pytesseract.image_to_string(PIL.Image.open(png_name), lang='dan'))
                        text = text.replace('-\n', '')
                        letters = list(string.ascii_lowercase)
                        letters.extend(['æ', 'ø', 'å'])

                        for letter in letters:
                            if letter in text:
                                has_text = 1

                        color_lst = ['brun', 'rød', 'sort',
                                     'grå', 'grøn', 'oliven',
                                     'hvid', 'blå', 'gul',
                                     'mørk', 'mangan', 'rust',
                                     'pyrit', 'jernudfældning',
                                     'okker', 'jern',
                                     'rødder', 'muld', 'blaa']

                        for color in color_lst:
                            if color in text:
                                has_color = 1
                                bh_color_lst.append(color)
                            else:
                                pass

                bh_color = ', '.join([str(col) for col in bh_color_lst])
                has_text_lst.append(has_text)
                has_color_lst.append(has_color)
                colors_lst.append(bh_color)

            os.remove(pdf_name)
            os.remove(png_name)
        df['has_text'] = has_text_lst
        df['has_color'] = has_color_lst
        df['colors'] = colors_lst
        df.to_sql('color_report_test', self.engine, schema='temp_simak', if_exists='append', index=False)


t1 = time.perf_counter()

PIL.Image.MAX_IMAGE_PIXELS = None

# define database query for borehole report urls in jupiter db
num_reports = 100
d = Database()
df_pdf_url = d.fetch_report_urls(num_reports)

num_threads = 4
df_list = []
for i in range(num_threads):
    fraction = 1/(num_threads-i)
    df = df_pdf_url.sample(frac=fraction)
    df_list.append(df)
    df_pdf_url = df_pdf_url.drop(df.index)

with concurrent.futures.ThreadPoolExecutor() as executer:
    executer.map(d.fetch_color_from_report, df_list)

t2 = time.perf_counter()
elapsed = round(t2 - t1)
print(f'\n{os.path.basename(__file__)} executed in {elapsed} s')

